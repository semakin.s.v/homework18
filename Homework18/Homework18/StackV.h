#pragma once
#include <iostream>
#include <vector>
class StackV
{
public:
	void push(int x)
	{
		size++;
		v.resize(size);
		v[size - 1] = x;
		std::cout << "Element added\n";
	}

	void pop()
	{
		if (size > 0)
		{
			size--;
			v.resize(size);
			std::cout << "Element removed\n";
		}
		else
		{
			std::cout << "Nothing to remove\n";
		}

	}

	void show()
	{
		for (auto &element : v)
			std::cout << element << " ";
		std::cout << "\n";
	}

private:
	int size = 0;
	std::vector<int> v;
};