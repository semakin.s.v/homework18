#include <iostream>
#include <vector>
#include <string>
#include "StackV.h"
#include "StackT.h"

int main()
{
	StackV ex;				//���������� ��� ������������� ��������

	ex.pop();

	ex.push(25);
	ex.show();

	ex.push(50);
	ex.push(8);
	ex.push(5);
	ex.push(250);
	ex.show();

	ex.pop();
	ex.show();

	ex.pop();
	ex.push(500);
	ex.pop();
	ex.push(8);
	ex.show();

	StackT<double> st;		//����������, ��������� �������

	st.push(0.8);
	st.push(46.8);
	st.push(10.8);
	st.pop();
	st.push(5.5);
	st.show();
	st.pop();
	st.push(8.9);
	st.show();

	StackT<std::string> st1;

	st1.push("One");
	st1.push("Two");
	st1.push("Three");
	st1.pop();
	st1.push("Four");
	st1.show();
	st1.pop();
	st1.push("Five");
	st1.show();

	
	/*int num;
	std::cout << "�nter the number of elements: ";
	std::cin >> num;
	std::cout << "\n";

	int* array = new int[num];
	for (int i = 0; i < num; i++)
	{
		array[i] = i;
		std::cout << array[i] << " ";
	}
	std::cout << "\n"; 

	
	delete [] array;
	array = nullptr;*/
	
}

